/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author BankMMT
 */
public class Cat extends Animal{
    public Cat(String name,String color){
        super(name,color,4);
        System.out.println("Cat created");
    }
    @Override
    public void walk(){
        System.out.printf("Cat: %s walk with %d legs.\n",name,legs);
    }
    @Override
    public void speak(){
        System.out.printf("Cat: %s speak >> meow meow!!\n",name);
    }
}
