/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author BankMMT
 */
public class Dog extends Animal{
    public Dog(String name, String color){
        super(name,color,4);
        System.out.println("Dog created");
    }
    
    @Override
    public void walk(){
        System.out.printf("Dog: %s walk with %d legs.\n",name,legs);
    }
    @Override
    public void speak(){
        System.out.printf("Dog: %s speak >> bark bark!!\n",name);
    }
}
