/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author BankMMT
 */
public class Duck extends Animal{
    private int wing;
    public Duck(String name,String color){
        super(name,color,2);
        System.out.println("Duck created");
    }
    public void fly(){
        System.out.printf("Duck : %s fly!!!\n",name);
    }
    @Override
    public void walk(){
        System.out.printf("Duck: %s walk with %d legs.\n",name,legs);
    }
    @Override
    public void speak(){
        System.out.printf("Duck: %s speak >> quak quak!!\n",name);
    }
}
