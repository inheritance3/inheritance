/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.inheritance;

/**
 *
 * @author BankMMT
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal ("Nan","Purple",0);
        animal.walk();
        animal.speak();
        System.out.println("------------------------------");
        
        Dog happy = new Dog ("Happy","White");
        happy.walk();
        happy.speak();
        System.out.println("------------------------------");
        
        Dog saiirung = new Dog ("Saiirung","Rainbow");
        saiirung.walk();
        saiirung.speak();
        System.out.println("------------------------------");
        
        Dog oil = new Dog ("Oil","Darkgreen");
        oil.walk();
        oil.speak();
        System.out.println("------------------------------");
        
        Dog mark = new Dog ("Mark","Blue");
        mark.walk();
        mark.speak();
        System.out.println("------------------------------");
        
        Cat jiajia = new Cat ("jiajia","Orange");
        jiajia.walk();
        jiajia.speak();
        System.out.println("------------------------------");
        
        Duck lemon = new Duck ("lemon","Yellow");
        lemon.walk();
        lemon.speak();
        lemon.fly();
        System.out.println("------------------------------");
        
        Duck nan = new Duck ("Nan","Purple");
        nan.walk();
        nan.speak();
        nan.fly();
        System.out.println("------------------------------");
        
        System.out.println("Happy is Animals : "+(happy instanceof Animal));
        System.out.println("Happy is Dog : "+(happy instanceof Dog));
        System.out.println("Happy is Object : "+(happy instanceof Object));
        System.out.println("Animal is Dog : "+(animal instanceof Dog));
        System.out.println("Animal is Animals : "+(animal instanceof Animal));
        
        System.out.println("Saiirung is Animals : "+(saiirung instanceof Animal));
        System.out.println("Saiirung is Dog : "+(saiirung instanceof Dog));
        System.out.println("Saiirung is Object : "+(saiirung instanceof Object));
        System.out.println("Animal is Dog : "+(animal instanceof Dog));
        System.out.println("Animal is Animals : "+(animal instanceof Animal));
        
        System.out.println("Oil is Animals : "+(oil instanceof Animal));
        System.out.println("Oil is Dog : "+(oil instanceof Dog));
        System.out.println("Oil is Object : "+(oil instanceof Object));
        System.out.println("Animal is Dog : "+(animal instanceof Dog));
        System.out.println("Animal is Animals : "+(animal instanceof Animal));
        
        System.out.println("Mark is Animals : "+(mark instanceof Animal));
        System.out.println("Mark is Dog : "+(mark instanceof Dog));
        System.out.println("Mark is Object : "+(mark instanceof Object));
        System.out.println("Animal is Dog : "+(animal instanceof Dog));
        System.out.println("Animal is Animals : "+(animal instanceof Animal));
        
        System.out.println("Jiajia is Animals : "+(jiajia instanceof Animal));
        System.out.println("Jiajia is Cat : "+(jiajia instanceof Cat));
        System.out.println("Jiajia is Object : "+(jiajia instanceof Object));
        System.out.println("Animal is Cat : "+(animal instanceof Cat));
        System.out.println("Animal is Animals : "+(animal instanceof Animal));
        
        System.out.println("Lemon is Animals : "+(lemon instanceof Animal));
        System.out.println("Lemon is Duck : "+(lemon instanceof Duck));
        System.out.println("Lemon is Object : "+(lemon instanceof Object));
        System.out.println("Animal is Duck : "+(animal instanceof Dog));
        System.out.println("Animal is Animals : "+(animal instanceof Animal));
        
        System.out.println("Nan is Animals : "+(nan instanceof Animal));
        System.out.println("Nan is Duck : "+(nan instanceof Duck));
        System.out.println("Nan is Object : "+(nan instanceof Object));
        System.out.println("Animal is Duck : "+(animal instanceof Duck));
        System.out.println("Animal is Animals : "+(animal instanceof Animal));
        

        Animal[] animals = {happy,saiirung,oil,mark,jiajia,lemon,nan};
        for (int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if (animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            
        }
    }
}
